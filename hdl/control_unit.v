module control_unit
(input clk, rst, start, 
output load, key_mux_control, encode_mux_control, key_shift_1, key_shift_2, ready, busy);

wire [4 : 0] counter_out;

counter counter (clk, start, rst, counter_out);

assign busy = ~((~counter_out[4] & ~counter_out[3] & ~counter_out[2] & ~counter_out[1] & ~counter_out[0]));
assign load = ((~counter_out[4] & counter_out[0]) | (~counter_out[4] & counter_out[1]) | (~counter_out[4] & counter_out[2]) | (~counter_out[4] & counter_out[3]) | (counter_out[4] & ~counter_out[3] & ~counter_out[2] & ~counter_out[1] & ~counter_out[0])) | start;
assign key_mux_control = ((~counter_out[4] & counter_out[1]) | (~counter_out[4] & counter_out[2]) | (~counter_out[4] & counter_out[3]) | (counter_out[4] & ~counter_out[3] & ~counter_out[2] & ~counter_out[1] & ~counter_out[0]));
assign key_shift_2 = ( (~counter_out[4] & counter_out[1] & counter_out[0]) | (~counter_out[4] & counter_out[2]) | (~counter_out[4] & counter_out[3] & ~counter_out[0])) ;
assign key_shift_1 = ((~counter_out[4] & counter_out[1]) | (~counter_out[4] & counter_out[2]) | (~counter_out[4] & counter_out[3]) | (counter_out[4] & ~counter_out[3] & ~counter_out[2] & ~counter_out[1] & ~counter_out[0])) | start;
assign encode_mux_control =  ((~counter_out[4] & counter_out[0]) | (~counter_out[4] & counter_out[1]) | (~counter_out[4] & counter_out[2]) | (~counter_out[4] & counter_out[3]));
assign ready = (~counter_out[4] & ~counter_out[3] & ~counter_out[2] & ~counter_out[1] & ~counter_out[0]);

endmodule