module counter 
(input clk, set, rst,
output reg [4 : 0] data_out);

always @(posedge clk) begin 

	if (rst == 1) begin 
		data_out <= 0;
	end else if (set == 1 && data_out == 0) begin 
		data_out <= 5'b10000;
	end else if (data_out == 0) begin 
		data_out <= data_out;
	end else begin
		data_out <= data_out - 1;
	end
		
end

endmodule
