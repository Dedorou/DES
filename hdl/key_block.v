module key_block
(input clk, rst,
input [0 : 63] data_in,   
input load, mux_control, shift_1, shift_2,
output [0 : 47] data_out);

wire [0 : 55] PC_1_block_out;
wire [0 : 27] mux_out_L, mux_out_R;
wire [0 : 27] shift_out_L, shift_out_R;
wire [0 : 55] reg_out;

PC_1_block PC_1_block (data_in, PC_1_block_out);
mux #(28) mux_L (PC_1_block_out[0 : 27], reg_out[0 : 27], mux_control, mux_out_L);
mux #(28) mux_R (PC_1_block_out[28 : 55], reg_out[28 : 55], mux_control, mux_out_R);
barrel_shift #(28) barrel_shift_L (mux_out_L, {shift_1, shift_2}, shift_out_L);
barrel_shift #(28) barrel_shift_R (mux_out_R, {shift_1, shift_2}, shift_out_R);
register #(56) register (clk, load, rst, {shift_out_L, shift_out_R}, reg_out);
PC_2_block PC_2_block (reg_out, data_out);

endmodule