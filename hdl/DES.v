module DES_module
(input clk, rst, start,
input [0 : 63] data_in, key,
output [0 : 63] data_out,
output ready, busy);

wire [0 : 47] sub_key;
wire load, encode_mux_control, key_mux_control, key_shift_1, key_shift_2;

control_unit control_unit (clk, rst, start, load, key_mux_control, encode_mux_control, key_shift_1, key_shift_2, ready, busy);
key_block key_block (clk, rst, key, load, key_mux_control, key_shift_1, key_shift_2, sub_key);
encryption_block encryption_block (clk, rst, data_in, sub_key, load, encode_mux_control, data_out);

endmodule