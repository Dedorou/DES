`timescale 1ns / 1ns


module read_test ();

parameter clk_period = 100;

integer test_counts;
integer test_file;
integer i;

reg [0 : 63] file_key, file_data_in, file_data_out;
reg clk = 0, rst = 0, start = 0;
wire [0 : 63] data_out;
wire ready, busy;

DES_module test (clk, rst, start, file_data_in, file_key, data_out, ready, busy);

always #(clk_period/2) clk=~clk;

initial begin

test_file=$fopen("F:/labs/DES/A_hex.txt","r");
#clk_period;
rst = 1;
#clk_period;
rst = 0;
#clk_period;

while (! $feof(test_file)) begin
	$fscanf(test_file,"%d\n", test_counts);
	for (i = 0; i < test_counts; i = i + 1) begin 
		$fscanf(test_file,"%h\n", file_data_in);
		$fscanf(test_file,"%h\n", file_key);
		$fscanf(test_file,"%h\n", file_data_out);
		#clk_period;
		start = 1;
		#clk_period;
		start = 0; 
		#(clk_period * 16);
		if (file_data_out == data_out) begin 
			$display("test %0d is correct", i+1);
		end else begin 
			$display("test %0d is uncorrect", i+1);
		end
	end
end
end

endmodule